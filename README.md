# grunt-init-gruntfile

> Cria um Gruntfile básico com [grunt-init][].

[grunt-init]: http://gruntjs.com/project-scaffolding

## Instalação
Se você ainda não tiver feito, instale o [grunt-init][].

Uma vez que o grunt-init esteja instalado, coloque o modelo no seu diretório`~/.grunt-init/` . É recomendável que você utilize git para clonar esse modelo para o diretório, como segue:

```
git clone https://bitbucket.org/xthor/grunt-init-gruntfile.git ~/.grunt-init/gruntfile
```

_(usuários Windows, utilize o Git Bash ou veja [a documentação][grunt-init] para saber o caminho correto do diretório destino)_

## Utilização

Na linha de comando, cd em um diretório vazio, execute este comando e siga as instruções.

```
grunt-init gruntfile
```

_Observe que este modelo irá gerar arquivos no diretório atual, portanto, certifique-se de mudar para um novo diretório primeiro se você não quiser sobrescrever arquivos existentes._
