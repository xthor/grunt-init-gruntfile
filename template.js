/*global exports*/
/*
 * grunt-init-gruntfile
 * https://gruntjs.com/
 *
 * Copyright (c) 2012 "Cowboy" Ben Alman, contributors
 * Licensed under the MIT license.
 */

'use strict';

// Basic template description.
exports.description = 'Cria um Gruntfile básico.';

// Template-specific notes to be displayed before question prompts.
exports.notes = 'Esse modelo se utiliza de um padrão para os caminhos de ' +
  'arquivos e diretórios, caso necessário edito o arquivo Gruntfile.js gerado ' +
  'antes de executar o grunt. _Se você executar o grunt depois de gerar o ' +
  'Gruntfile, e for apresentado erros, edite o arquivo_';

// Any existing file or directory matching this wildcard will cause a warning.
exports.warnOn = 'Gruntfile.js';

// The actual init template.
exports.template = function(grunt, init, done) {

  init.process({}, [
    // Prompt for these values.
  ], function(err, props) {
    props.file_name = '<%= pkg.name %>';

    // Files to copy (and process).
    var files = init.filesToCopy(props);

    // Actually copy (and process) files.
    init.copyAndProcess(files, props);

    // All done!
    done();
  });

};
