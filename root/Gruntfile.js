/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        options: {
          sourcemap: 'none',
          style: 'compressed',
          precision: 8
        },
        files: {
          'style.css': 'style.scss'
        }
      }
    },
    watch: {
      sass: {
        files: '*.scss',
        tasks: ['sass']
      }
    }
  });

  // These plugins provide necessary tasks.{
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task.
  grunt.registerTask('default', ['watch']);

};
